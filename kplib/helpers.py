#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def from_pymatgen(structure):
    lattice = structure.lattice.matrix
    positions = structure.frac_coords
    numbers = structure.atomic_numbers
    cell = (lattice, positions, numbers)
    return cell
