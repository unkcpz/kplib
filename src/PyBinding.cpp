#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "kPointLatticeGenerator.h"
#include "kPointLattice.h"


namespace py = pybind11;

PYBIND11_MODULE(_kplib, m) {

  m.doc() = "kpLib C++ interface";


 	py::enum_<INCLUDE_GAMMA> (m,"INCLUDE_GAMMA")
 		.value("TRUE",INCLUDE_GAMMA::TRUE)
		.value("FALSE",INCLUDE_GAMMA::FALSE)
		.value("AUTO",INCLUDE_GAMMA::AUTO);


	py::class_<KPointLattice> (m,"KPointLattice")
		.def("get_super_to_direct",&KPointLattice::getSuperToDirect)
		.def("get_shift",&KPointLattice::getShift)
		.def("get_min_periodic_distance",&KPointLattice::getMinPeriodicDistance)
		.def("get_num_distinct_kpoints",&KPointLattice::getNumDistinctKPoints)
		.def("get_num_total_kpoints", &KPointLattice::numTotalKPoints)
		.def("get_kpoint_coordinates", &KPointLattice::getKPointCoordinates)
		.def("get_kpoint_weights", &KPointLattice::getKPointWeights);


 	py::class_<KPointLatticeGenerator> (m, "KPointLatticeGenerator")
 	  .def(py::init<Tensor<double>,Tensor<double>,std::vector<Tensor<int>>,const bool>())
    .def("get_kpoint_lattice", (KPointLattice (KPointLatticeGenerator::*)(const double, const int))  &KPointLatticeGenerator::getKPointLattice)
 	  .def("include_gamma",&KPointLatticeGenerator::includeGamma)
 	  .def("use_scale_factor", &KPointLatticeGenerator::useScaleFactor);

}
